﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication62
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void getlist()
        {
            Model1Container contex = new Model1Container();
            dataGridView1.DataSource = contex.studentSet.ToList();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            getlist();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model1Container contex = new Model1Container();
            contex.studentSet.Add(new student
            {
                Name = textBox1.Text,
                LastName = textBox2.Text,
                Age = Convert.ToInt32(textBox3.Text)
            
            });
            contex.SaveChanges();
            getlist();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Model1Container contex = new Model1Container();
            student st = new student();
            st.Id = Convert.ToInt32(textBox4.Text);
            contex.studentSet.Attach(st);
            contex.studentSet.Remove(st);
            contex.SaveChanges();
            getlist();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Model1Container contex = new Model1Container();
            student st = new student();
            st.Id = Convert.ToInt32(textBox4.Text);
            contex.studentSet.Attach(st);
            st.Name = textBox1.Text;
            st.LastName = textBox2.Text;
            st.Age = Convert.ToInt32(textBox3.Text);
            contex.SaveChanges();
            getlist();
        }
    }
}
