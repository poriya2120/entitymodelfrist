
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/05/2021 19:05:10
-- Generated from EDMX file: C:\Users\poriya\Documents\Visual Studio 2015\Projects\WindowsFormsApplication62\WindowsFormsApplication62\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [db_model];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_studentSelectCourse]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SelectCourseSet] DROP CONSTRAINT [FK_studentSelectCourse];
GO
IF OBJECT_ID(N'[dbo].[FK_CourseSelectCourse]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SelectCourseSet] DROP CONSTRAINT [FK_CourseSelectCourse];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[studentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[studentSet];
GO
IF OBJECT_ID(N'[dbo].[CourseSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CourseSet];
GO
IF OBJECT_ID(N'[dbo].[SelectCourseSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SelectCourseSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'studentSet'
CREATE TABLE [dbo].[studentSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [Age] int  NOT NULL
);
GO

-- Creating table 'CourseSet'
CREATE TABLE [dbo].[CourseSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [title] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'SelectCourseSet'
CREATE TABLE [dbo].[SelectCourseSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [studentId] int  NOT NULL,
    [CourseId] int  NOT NULL,
    [number] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'studentSet'
ALTER TABLE [dbo].[studentSet]
ADD CONSTRAINT [PK_studentSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CourseSet'
ALTER TABLE [dbo].[CourseSet]
ADD CONSTRAINT [PK_CourseSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SelectCourseSet'
ALTER TABLE [dbo].[SelectCourseSet]
ADD CONSTRAINT [PK_SelectCourseSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [studentId] in table 'SelectCourseSet'
ALTER TABLE [dbo].[SelectCourseSet]
ADD CONSTRAINT [FK_studentSelectCourse]
    FOREIGN KEY ([studentId])
    REFERENCES [dbo].[studentSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_studentSelectCourse'
CREATE INDEX [IX_FK_studentSelectCourse]
ON [dbo].[SelectCourseSet]
    ([studentId]);
GO

-- Creating foreign key on [CourseId] in table 'SelectCourseSet'
ALTER TABLE [dbo].[SelectCourseSet]
ADD CONSTRAINT [FK_CourseSelectCourse]
    FOREIGN KEY ([CourseId])
    REFERENCES [dbo].[CourseSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CourseSelectCourse'
CREATE INDEX [IX_FK_CourseSelectCourse]
ON [dbo].[SelectCourseSet]
    ([CourseId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------